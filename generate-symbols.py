
# This script is meant to run at the end of the following pipeline:
#   <keymap.xkb grep -o '│.*│' | sed 's/ //g' | python3 generate-symbols.py

import fileinput

# This script expects on sdin: eight lines containing groups of exactly four 
# characters, separated by │ symbols, every two lines standing for one of the 
# rows below:

codes = [ 
    [ 'TLDE', 'AE01', 'AE02', 'AE03', 'AE04', 'AE05', 'AE06', 'AE07', 'AE08', 'AE09', 'AE10', 'AE11', 'AE12' ],
    [ 'LatQ', 'LatW', 'LatE', 'LatR', 'LatT', 'LatY', 'LatU', 'LatI', 'LatO', 'LatP', 'AD11', 'AD12' ],
    [ 'LatA', 'LatS', 'LatD', 'LatF', 'LatG', 'LatH', 'LatJ', 'LatK', 'LatL', 'AC10', 'AC11', 'BKSL' ],
    [ 'LatZ', 'LatX', 'LatC', 'LatV', 'LatB', 'LatN', 'LatM', 'AB08', 'AB09', 'AB10' ] ]

# split input into four-character groups
lines = [ line[1:-2].split('│') for line in fileinput.input() ]

# combine upper and lower case
symbols = [ [ [*zip(lower,upper)]
    for (upper,lower) in zip(*line) ]
        for line in zip(lines[::2], lines[1::2]) ]

# Sorry for speaking in occult incantations like the above. After looking into 
# the inner workings of XKB, just about anything looks straightforward.

if list(map(len,codes)) != list(map(len, symbols)):
    print("Mismatch in number of keys and symbols.")
    exit(1)

# flatten and combine keys and symbols
keys = [ (code,symbol) for row in zip(codes,symbols) for (code,symbol) in zip(*row) ]

print('''
// This file has been automatically generated; changes you make might not persist.

default partial alphanumeric_keys
xkb_symbols "generated" {
''')

deads = {
    '\u0300' : 'dead_grave',
    '\u0301' : 'dead_acute',
    '\u0302' : 'dead_circumflex',
    '\u0303' : 'dead_tilde',
    '\u0304' : 'dead_macron',
    '\u0306' : 'dead_breve',
    '\u0307' : 'dead_abovedot',
    '\u0308' : 'dead_diaeresis',
    '\u0313' : 'dead_psili',
    '\u0314' : 'dead_dasia',
    '\u030a' : 'dead_abovering',
    '\u030b' : 'dead_doubleacute',
    '\u030c' : 'dead_caron',
    '\u031b' : 'dead_horn',
    '\u0323' : 'dead_belowdot',
    '\u0327' : 'dead_cedilla',
    '\u0328' : 'dead_ogonek',
    '\u0345' : 'dead_iota',
}

def make_code(char):
    return "NoSymbol" if char == '�' else deads.get(char) or f" U{ord(char):04X}  "
def make_key(lower, upper):
    return f'[{make_code(lower)},{make_code(upper)}]'
def make_chars(a,b):
    return ' '.join([*a, *b])

for (key, symbols) in keys:
    g1, g2, g3, g4 = symbols
    chars = ' '.join([*g1, *g2, *g3, *g4])
    print(f'    key <{key}> {{ {make_key(*g1)}, {make_key(*g2)},  // {make_chars(g1,g2)}')
    print(f'                 {make_key(*g3)}, {make_key(*g4)}}}; // {make_chars(g3,g4)}')

print(f'''
}};
// {'vim'}: syntax=xkb
''')


