
.PHONY: apply compile check-compositions xcape-on xcape-off
compile : keymap-compiled.xkm keymap-uncompiled.xkb README.html

keymap-compiled.xkm : keymap.xkb symbols/generated
	xkbcomp -w 3 -I. keymap.xkb keymap-compiled.xkm

keymap-uncompiled.xkb : keymap.xkb symbols/generated
	xkbcomp -w 3 -xkb -I. keymap.xkb keymap-uncompiled.xkb

symbols/generated : keymap.xkb generate-symbols.py Makefile
	<keymap.xkb grep -o '│.*│' | sed 's/ //g' | python3 generate-symbols.py > symbols/generated

README.html : README.md
	pandoc -f gfm -t html README.md > README.html

apply : compile xcape-off
	xkbcomp -w 3 keymap-compiled.xkm ${DISPLAY}

check-compositions :
	cat compose/* | grep '^<Multi_key> *' | sed 's/ //g;s/#.*//;s/^[^>]*>//' | LC_ALL=C sort |\
		awk -F : 'BEGIN { prev = "." } {\
			if (prev == substr($$1, 0, length(prev)))\
				print prev, sym, $$1, $$2 ;\
			prev = $$1; sym = $$2 }' |\
		column -t

xcape-off : keymap-compiled.xkm
	pkill xcape || true

# Running the following will have:
#  - the Capslock key generate Escape instead of Control if pressed alone;
#  - the Return key generate Control if pressed together with another key.
# As a side effect, this will disable key repeat on the Return key.
xcape-on : apply xcape-off
	xmodmap -e 'keysym Return = Control_R Control_R'
	xmodmap -e 'add Control = Control_R'
	xmodmap -e 'keycode any = Return'
	xcape -t 250 -e 'Control_L=Escape;Control_R=Return'

