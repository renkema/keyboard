<h1 id="synopsis">SYNOPSIS</h1>
<p>Multi-lingual keyboard configuration for theologians. It allows simultaneous input of Hebrew, Greek and German text and contains a large selection of mathematical symbols.</p>
<h1 id="installation">INSTALLATION</h1>
<p>Open a terminal and run the following lines:</p>
<pre><code>git clone https://gitlab.com/renkema/keyboard.git ~/.config/keyboard/
ln ~/.config/keyboard/XCompose ~/.XCompose
</code></pre>
<p>You can now find the configuration files in the <code>~/.config/keyboard/</code> directory.</p>
<p>Apply the settings by saying:</p>
<pre><code>cd ~/.config/keyboard/ ; make apply
</code></pre>
<p>You will need restart your terminal before seeing the effect, since applications that are already open will not be affected. The settings will persist until you logout; make them permanent by adding the following lines to the beginning of your <code>~/.xsession</code> configuration file:</p>
<pre><code>export GTK_IM_MODULE=xim
export QT_IM_MODULE=xim
export XMODIFIERS=@im=none
xkbcomp $HOME/.config/keyboard/keymap-compiled.xkm $DISPLAY
</code></pre>
<p>(Without the first three lines your keyboard will work, but your Compose key might not: some applications apparently do not read the <code>~/.XCompose</code> file.)</p>
<h1 id="usage">USAGE</h1>
<p>Your keyboard will now have the following modifier keys:</p>
<table>
<thead>
<tr class="header">
<th style="text-align: left;">Old key</th>
<th style="text-align: left;">New meaning</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">Capslock</td>
<td style="text-align: left;">Control</td>
</tr>
<tr class="even">
<td style="text-align: left;">Left control</td>
<td style="text-align: left;">Symbol</td>
</tr>
<tr class="odd">
<td style="text-align: left;">Left alt</td>
<td style="text-align: left;">Greek</td>
</tr>
<tr class="even">
<td style="text-align: left;">Right alt</td>
<td style="text-align: left;">Hebrew</td>
</tr>
<tr class="odd">
<td style="text-align: left;">Menu</td>
<td style="text-align: left;">Compose</td>
</tr>
<tr class="even">
<td style="text-align: left;">Right control</td>
<td style="text-align: left;">Symbol</td>
</tr>
</tbody>
</table>
<h2 id="greek-hebrew-and-symbol-keys">Greek, Hebrew and Symbol keys</h2>
<p>These keys will shift the keyboard to a different alphabet. Press them together with another character: <code>Greek+Shift+g</code> should give <code>Γ</code>. You can use the control key together with one of these to lock it: after typing <code>Control+Hebrew</code> you can continue typing in Hebrew without pressing the Hebrew key again. If you have switched to another keyboard, you can use its own key for going back to Latin again (so that, after pressing <code>Control+Hebrew</code>, <code>Hebrew+a</code> will give <code>a</code> and <code>Control+Hebrew</code> switches back to Latin). If you are confused about the state your keyboard is in, you can always use <code>Control+Space</code> for switching back to Latin.</p>
<p>You can see the full list of symbols accessible to you via the keys on the keyboard drawn at the top of the file <code>~/.config/keyboard/keymap.xkb</code>. (See under MODIFICATION below for how to change them.) In a properly-configured editor or terminal, this should look like this:</p>
<p><img src="./keyboard.png" alt="screenshot of keyboard configuration" /></p>
<h2 id="compose-key">Compose key</h2>
<p>The Compose key can be used for combining several characters into one. Use it by first pressing and releasing the Compose key and then, in sequence, pressing and releasing the characters you want to compose. For instance: <code>Compose " a</code> should give <code>ä</code> and <code>Compose / o</code> should give <code>ø</code>.</p>
<p>Compose sequences are included (among others) for accented characters (both Latin and Greek), Hebrew cantillation marks, all kinds of mathematical symbols, critical apparatus markings, box drawing characters, arrows, phonetic symbols, currency, bullet markers, quotation marks, vulgar fractions and chess pieces.</p>
<p>All composeable sequences are defined in the <code>~/.config/keyboard/compose/</code> directory. The overview below is incomplete; see the top of each file for a more elaborate listing.</p>
<table>
<tbody>
<tr class="odd">
<td style="text-align: left;"><code>^</code></td>
<td style="text-align: left;">ô</td>
<td style="text-align: left;"><code>.</code></td>
<td style="text-align: left;">ȯ</td>
<td style="text-align: left;"><code>'</code></td>
<td style="text-align: left;">ó *</td>
<td style="text-align: left;"><code>u</code></td>
<td style="text-align: left;">ŏ *</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>"</code></td>
<td style="text-align: left;">ö</td>
<td style="text-align: left;"><code>o</code></td>
<td style="text-align: left;">å</td>
<td style="text-align: left;"><code>`</code></td>
<td style="text-align: left;">ò *</td>
<td style="text-align: left;"><code>[</code></td>
<td style="text-align: left;">ἁ ‡</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>v</code></td>
<td style="text-align: left;">ǒ</td>
<td style="text-align: left;"><code>=</code></td>
<td style="text-align: left;">ő</td>
<td style="text-align: left;"><code>~</code></td>
<td style="text-align: left;">õ *</td>
<td style="text-align: left;"><code>]</code></td>
<td style="text-align: left;">ἀ ‡</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>,</code>  </td>
<td style="text-align: left;">ç  </td>
<td style="text-align: left;"><code>/</code>  </td>
<td style="text-align: left;">ø †  </td>
<td style="text-align: left;"><code>_</code>  </td>
<td style="text-align: left;">ō *  </td>
<td style="text-align: left;"><code>ι</code>  </td>
<td style="text-align: left;">ᾳ ‡  </td>
</tr>
</tbody>
</table>
<p>First, an overview of accents. For many accents, a combining variant can be input by pressing the Compose key twice and then the accent character. (*) These can also be used with greek; (†) the slash can be used with many nonvowels, e.g. ≠, ł, đ, ¢ etc; (‡) the iota subscriptum must be input first and the spiritus last, with other accents inbetween.</p>
<p>Secondly, many digraphs can be given as-they-are, if that would not collide with a character used for accents as above: <code>ae</code>, <code>th</code>, <code>i.</code> and <code>ng</code> give æ, þ, ı and ŋ. Otherwise, many variant symbols can be given by affixing a semicolon: <code>d;</code>, <code>e;</code>, <code>**;</code>, <code>s;</code> and <code>ε;</code> give ð, ə, ‡, ß and ϵ. (Note that <code>dh</code> gives ḥ, so that <code>d;</code> must be used for ð.) Some characters can also be repeated for the same effect.</p>
<p>Then, many symbol classes have a handy mnemonic:</p>
<table>
<thead>
<tr class="header">
<th style="text-align: left;">Group</th>
<th style="text-align: left;">Prefix</th>
<th style="text-align: left;">In</th>
<th style="text-align: left;">Out</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">Planets</td>
<td style="text-align: left;"><code>p</code></td>
<td style="text-align: left;"><code>p0</code> <code>p1</code> etc.</td>
<td style="text-align: left;">☉ ☿ ♁ ♂ ♃ ♄ ♅ ♆</td>
</tr>
<tr class="even">
<td style="text-align: left;">Chess pieces</td>
<td style="text-align: left;"><code>c</code></td>
<td style="text-align: left;"><code>ck</code> <code>cN</code> etc.</td>
<td style="text-align: left;">♔ ♞ etc.</td>
</tr>
<tr class="odd">
<td style="text-align: left;">Warning signs</td>
<td style="text-align: left;"><code>!</code></td>
<td style="text-align: left;"><code>!x</code> <code>!r</code> and <code>!b</code></td>
<td style="text-align: left;">☠︎ ☢︎ ☣︎</td>
</tr>
<tr class="even">
<td style="text-align: left;">Musical symbols</td>
<td style="text-align: left;"><code>#</code></td>
<td style="text-align: left;"><code>#b</code> <code>##</code> <code>#G</code></td>
<td style="text-align: left;">♭ ♯ 𝄞</td>
</tr>
<tr class="odd">
<td style="text-align: left;">Suits</td>
<td style="text-align: left;"><code>&lt;</code> or <code>&gt;</code></td>
<td style="text-align: left;"><code>&lt;2</code> <code>&lt;3</code> <code>&gt;2</code> <code>&gt;3</code></td>
<td style="text-align: left;">♠︎ ♡ ♤ ♥︎</td>
</tr>
<tr class="even">
<td style="text-align: left;">Fractions</td>
<td style="text-align: left;"></td>
<td style="text-align: left;"><code>12</code> <code>45</code></td>
<td style="text-align: left;">½ ⅘</td>
</tr>
<tr class="odd">
<td style="text-align: left;">High quotes</td>
<td style="text-align: left;"><code>[</code> or <code>]</code></td>
<td style="text-align: left;"><code>['</code> <code>]"</code></td>
<td style="text-align: left;">‘ ”</td>
</tr>
<tr class="even">
<td style="text-align: left;">Other quotes</td>
<td style="text-align: left;"><code>.</code> or doubling</td>
<td style="text-align: left;"><code>&lt;&lt;</code> <code>.&lt;</code> <code>,,</code> <code>.,</code></td>
<td style="text-align: left;">« ‹ „ ‚</td>
</tr>
<tr class="odd">
<td style="text-align: left;">Brackets</td>
<td style="text-align: left;"><code>[</code> or <code>]</code></td>
<td style="text-align: left;"><code>[[</code> <code>[|</code> <code>[f</code> <code>[c</code></td>
<td style="text-align: left;">⟨ ⟦ ⌊ ⌈</td>
</tr>
<tr class="even">
<td style="text-align: left;">Sub- or superscripts</td>
<td style="text-align: left;"><code>_</code> or <code>^</code></td>
<td style="text-align: left;"><code>^n</code> <code>_1</code></td>
<td style="text-align: left;">ⁿ ₁</td>
</tr>
<tr class="odd">
<td style="text-align: left;">Box-drawing characters</td>
<td style="text-align: left;"><code>hjkl</code> with <code>-</code></td>
<td style="text-align: left;"><code>hjkl-</code> <code>HJLK-</code> <code>HkL-</code></td>
<td style="text-align: left;">- ╬ ╧</td>
</tr>
<tr class="even">
<td style="text-align: left;">Mathematical operators</td>
<td style="text-align: left;"><code>wo</code></td>
<td style="text-align: left;"><code>woi</code> <code>wop</code> <code>woe</code> <code>wov</code></td>
<td style="text-align: left;">∫ ∏ ∃ √</td>
</tr>
<tr class="odd">
<td style="text-align: left;">U- and V-like symbols</td>
<td style="text-align: left;"><code>wu</code> or <code>wv</code> with <code>hjkl</code></td>
<td style="text-align: left;"><code>wul</code> <code>wuj</code> <code>wuJ</code> <code>weh</code></td>
<td style="text-align: left;">⊃ ∪ ⋃ ∈</td>
</tr>
<tr class="even">
<td style="text-align: left;">Mathematical variables</td>
<td style="text-align: left;"><code>w</code> with one of <code>iIsSbBfF</code></td>
<td style="text-align: left;"><code>wiA</code> <code>wIA</code> etc.</td>
<td style="text-align: left;">𝐴 𝑨 𝒜 𝓐 𝐀 𝔸 𝔄 𝕬</td>
</tr>
<tr class="odd">
<td style="text-align: left;">Other mathematical</td>
<td style="text-align: left;"><code>w</code></td>
<td style="text-align: left;"><code>wh</code> <code>wd</code> <code>wn</code> <code>w8</code></td>
<td style="text-align: left;">ℏ ∂ ∇ ∞</td>
</tr>
<tr class="even">
<td style="text-align: left;">Text-critical marks</td>
<td style="text-align: left;"><code>A</code></td>
<td style="text-align: left;"><code>At</code> <code>As</code> <code>A.s</code> <code>A[s</code> <code>Ao</code></td>
<td style="text-align: left;">⸈ ⸀ ⸁ ⸂ ⸋</td>
</tr>
<tr class="odd">
<td style="text-align: left;">Cantillation</td>
<td style="text-align: left;">first letters of name</td>
<td style="text-align: left;"><code>את</code> <code>זק</code> <code>זג</code></td>
<td style="text-align: left;">א֑ א֔ א֕</td>
</tr>
</tbody>
</table>
<p>Bullet-like symbols can be made with combinations of <code>oOμmM</code> (round and square, smaller to larger) and <code>-=</code> (open and closed). Currency symbols can be made by affixing or prefixing letters with <code>-</code> or <code>=</code>. Arrows can be drawn: <code>-&gt;</code>, <code>&lt;=</code>, <code>|&gt;</code> give →, ⇐ and ↦ (double arrows go with <code>&lt;&gt;</code> and <code>==</code>; up and down with <code>^</code> and <code>v</code>). Finally, note again that many variant or lookalike characters can be made with the <code>;</code> suffix.</p>
<h1 id="modification">MODIFICATION</h1>
<p>All key bindings are defined in <code>keymap.xkb</code>. At the top of the file, you will find a drawing of a keyboard. Every key contains two rows of four characters. You can replace these as you like. (But do note that every key should contain exactly eight characters.)</p>
<p>Lower in the file, under <code>xkb_symbols</code>, you can modify the assignment of modifier keys.</p>
<p>After making changes to the configuration, say <code>make</code> to compile the keyboard and <code>make apply</code> to compile and also apply the new settings. (You must first <code>cd</code> into the directory and then run <code>make</code>.)</p>
<p>Changes you make to the compose sequences will take effect after you log out and back in again (or possibly after re-starting the program you are working in). Do note that a compose sequence will not work if there is another sequence that starts with the same characters, in the same order. Say <code>make check-compositions</code> to check for clashes.</p>
<p>For names of characters, find the file named <code>keysymdef.h</code> (you can ask your terminal <code>locate keysymdef.h</code>).</p>
<p>A useful utility for examining unicode code points is avaliable on Debian-based systems in the <code>unicode</code> package. Furthermore, the <code>x11-utils</code> package contains the <code>xev</code> utility that can be used for examining keyboard events.</p>
<h1 id="fonts-and-terminals">FONTS AND TERMINALS</h1>
<p>You will need fonts that include many symbols and a terminal emulator that can display right-to-left text. The only terminal I know of that does this well is <code>mlterm</code>, but it can be hard to configure. I include these notes here, because proper documentation appears not to exist.</p>
<p>In particular, you should take care to always use the following options together. Either use anti-aliased fonts and have (in the ‘<code>main</code>’ configuration file):</p>
<pre><code>type_engine=cairo
use_anti_alias=true
line_space=4
use_ot_layout=true
</code></pre>
<p>Or do not use anti-aliasing and then use bitmap fonts with the following settings:</p>
<pre><code>type_engine=xcore
use_anti_alias=false
line_space=0
use_ot_layout=false
</code></pre>
<p>On Debian-based distributions, good fixed-width bitmap terminal fonts are available in the <code>xfonts-75dpi</code> and <code>xfonts-100dpi</code> packages. Use them with the second set of options above in the ‘<code>main</code>’ configuration file and in the ‘<code>font</code>’ configuration file the following:</p>
<pre><code>ISO10646_UCS4_1 = -misc-fixed-medium-r-semicondensed-*-13-*-*-*-*-*-iso10646-*;
</code></pre>
<p>For anti-aliased fonts, use the first set of options, with the following in the ‘<code>aafont</code>’ configuration file:</p>
<pre><code>ISO10647_UCS4_1 = DejaVu Sans Mono;
U+0590-05FF = Ezra SIL;
</code></pre>
<p>I also recommend the following settings (all in the ‘<code>main</code>’ configuration file):</p>
<pre><code>bidi_mode=left
bg_color=black
fg_color=white
compose_dec_special_font=true
copy_paste_via_ucs=false
only_use_unicode_font=true
receive_string_via_ucs=true
encoding=UTF-8
regard_uri_as_word=true
tabsize=4
termtype=st-256color
use_bidi=true
use_combining=true
use_ctl=true
use_scrollbar=false
</code></pre>
