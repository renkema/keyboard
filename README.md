
SYNOPSIS
========

Multi-lingual keyboard configuration for theologians. It allows simultaneous 
input of Hebrew, Greek and German text and contains a large selection of 
mathematical symbols.


INSTALLATION
============

Open a terminal and run the following lines:

    git clone https://gitlab.com/renkema/keyboard.git ~/.config/keyboard/
    ln ~/.config/keyboard/XCompose ~/.XCompose

You can now find the configuration files in the
`~/.config/keyboard/` directory.

Apply the settings by saying:

    cd ~/.config/keyboard/ ; make apply

You will need restart your terminal before seeing the effect,
since applications that are already open will not be affected.
The settings will persist until you logout;
make them permanent by adding
the following lines to the beginning of your `~/.xsession` configuration file:

    export GTK_IM_MODULE=xim
    export QT_IM_MODULE=xim
    export XMODIFIERS=@im=none
    xkbcomp $HOME/.config/keyboard/keymap-compiled.xkm $DISPLAY

(Without the first three lines your keyboard will work, but your Compose key 
might not: some applications apparently do not read the `~/.XCompose` file.)


USAGE
=====

Your keyboard will now have the following modifier keys:

| Old key       | New meaning |
| :---          | :---        |
| Capslock      | Control     |
| Left control  | Symbol      |
| Left alt      | Greek       |
| Right alt     | Hebrew      |
| Menu          | Compose     |
| Right control | Symbol      |


Greek, Hebrew and Symbol keys
-----------------------------

These keys will shift the keyboard to a different alphabet. Press them together 
with another character: `Greek+Shift+g` should give `Γ`. You can use the 
control key together with one of these to lock it: after typing 
`Control+Hebrew` you can continue typing in Hebrew without pressing the Hebrew 
key again. If you have switched to another keyboard, you can use its own key 
for going back to Latin again (so that, after pressing `Control+Hebrew`, 
`Hebrew+a` will give `a` and `Control+Hebrew` switches back to Latin). If you 
are confused about the state your keyboard is in, you can always use 
`Control+Space` for switching back to Latin.

You can see the full list of symbols accessible to you via the keys on the 
keyboard drawn at the top of the file `~/.config/keyboard/keymap.xkb`. (See 
under MODIFICATION below for how to change them.)
In a properly-configured editor or terminal, this should look like this:

![screenshot of keyboard configuration](./keyboard.png)


Compose key
-----------

The Compose key can be used for combining several characters into one. Use it 
by first pressing and releasing the Compose key and then, in sequence, pressing 
and releasing the characters you want to compose.
For instance: `Compose " a` should give `ä` and `Compose / o` should give `ø`.

Compose sequences are included (among others) for accented characters (both 
Latin and Greek), Hebrew cantillation marks, all kinds of mathematical symbols, 
critical apparatus markings, box drawing characters, arrows, phonetic symbols, 
currency, bullet markers, quotation marks, vulgar fractions and chess pieces.

All composeable sequences are defined in the `~/.config/keyboard/compose/` 
directory. The overview below is incomplete; see the top of each file for 
a more elaborate listing.

|       |      |       |       |         |       |       |       |
| :---  | :--- | :---  | :---  | :---    | :---  | :---  | :---  |
| `^`   | ô    | `.`   | ȯ     | `'`     | ó *   | `u`   | ŏ *   |
| `"`   | ö    | `o`   | å     | `` ` `` | ò *   | `[`   | ἁ ‡   |
| `v`   | ǒ    | `=`   | ő     | `~`     | õ *   | `]`   | ἀ ‡   |
| `,`   | ç    | `/`   | ø †   | `_`     | ō *   | `ι`   | ᾳ ‡   |

First, an overview of accents. For many accents, a combining variant can be 
input by pressing the Compose key twice and then the accent character. (\*) 
These can also be used with greek; (†) the slash can be used with many 
nonvowels, e.g. ≠, ł, đ, ¢ etc; (‡) the iota subscriptum must be input first 
and the spiritus last, with other accents inbetween.

Secondly, many digraphs can be given as-they-are, if that would not collide 
with a character used for accents as above: `ae`, `th`, `i.` and `ng` give æ, 
þ, ı and ŋ. Otherwise, many variant symbols can be given by affixing 
a semicolon: `d;`, `e;`, `**;`, `s;` and `ε;` give ð, ə, ‡, ß and ϵ. (Note that 
`dh` gives ḥ, so that `d;` must be used for ð.) Some characters can also be 
repeated for the same effect.

Then, many symbol classes have a handy mnemonic:

| Group           | Prefix                 | In                  | Out      |
| :---            | :---                   | :---                | :---     |
| Planets         | `p`                    | `p0` `p1` etc.      | ☉ ☿ ♁ ♂ ♃ ♄ ♅ ♆ |
| Chess pieces    | `c`                    | `ck` `cN` etc.      | ♔ ♞ etc. |
| Warning signs   | `!`                    | `!x` `!r` and `!b`  | ☠︎ ☢︎ ☣︎    |
| Musical symbols | `#`                    | `#b` `##` `#G`      | ♭ ♯ 𝄞    |
| Suits           | `<` or `>`             | `<2` `<3` `>2` `>3` | ♠︎ ♡ ♤ ♥︎  |
| Fractions       |                        | `12` `45`           | ½ ⅘      |
| High quotes     | `[` or `]`             | `['` `]"`           | ‘ ”      |
| Other quotes    | `.` or doubling        | `<<` `.<` `,,` `.,` | « ‹ „ ‚  |
| Brackets        | `[` or `]`             | `[[` `[\|`  `[f` `[c` | ⟨ ⟦ ⌊ ⌈ |
| Sub- or superscripts   | `_` or `^`      | `^n` `_1`                | ⁿ ₁      |
| Box-drawing characters | `hjkl` with `-` | `hjkl-` `HJLK-` `HkL-`   | - ╬ ╧    |
| Mathematical operators | `wo`            | `woi` `wop` `woe` `wov`  | ∫ ∏ ∃ √  |
| U- and V-like symbols  | `wu` or `wv` with `hjkl`   | `wul` `wuj`  `wuJ` `weh` | ⊃ ∪ ⋃ ∈  |
| Mathematical variables | `w` with one of `iIsSbBfF` | `wiA` `wIA` etc.   | 𝐴 𝑨 𝒜 𝓐 𝐀 𝔸 𝔄 𝕬 |
| Other mathematical | `w`         | `wh` `wd` `wn` `w8`      | ℏ ∂ ∇ ∞  |
| Text-critical marks    | `A`             | `At` `As` `A.s` `A[s` `Ao` | ⸈ ⸀ ⸁ ⸂ ⸋ |
| Cantillation    | first letters of name | `את` `זק` `זג`      | א֑ א֔ א֕    |

Bullet-like symbols can be made with combinations of `oOμmM` (round and square, 
smaller to larger) and `-=` (open and closed). Currency symbols can be made by 
affixing or prefixing letters with `-` or `=`. Arrows can be drawn: `->`, `<=`, 
`|>` give →, ⇐ and ↦ (double arrows go with `<>` and `==`; up and down with `^` 
and `v`). Finally, note again that many variant or lookalike characters can be 
made with the `;` suffix.



MODIFICATION
============

All key bindings are defined in `keymap.xkb`. At the top of the file, you will 
find a drawing of a keyboard. Every key contains two rows of four characters. 
You can replace these as you like. (But do note that every key should contain 
exactly eight characters.)

Lower in the file, under `xkb_symbols`, you can modify the assignment of 
modifier keys.

After making changes to the configuration, say `make` to compile the keyboard 
and `make apply` to compile and also apply the new settings. (You must first 
`cd` into the directory and then run `make`.)

Changes you make to the compose sequences will take effect after you log out 
and back in again (or possibly after re-starting the program you are working 
in).
Do note that a compose sequence will not work if there is another sequence that 
starts with the same characters, in the same order.
Say `make check-compositions` to check for clashes.

For names of characters, find the file named `keysymdef.h` (you can ask your 
terminal `locate keysymdef.h`).

A useful utility for examining unicode code points is avaliable on Debian-based 
systems in the `unicode` package.
Furthermore, the `x11-utils` package contains the `xev` utility that can be 
used for examining keyboard events.


FONTS AND TERMINALS
===================

You will need fonts that include many symbols and a terminal emulator that can 
display right-to-left text. The only terminal I know of that does this well is 
`mlterm`, but it can be hard to configure. I include these notes here, because 
proper documentation appears not to exist.

In particular, you should take care to always use the following options 
together. Either use anti-aliased fonts and have (in the ‘`main`’ configuration 
file):

    type_engine=cairo
    use_anti_alias=true
    line_space=4
    use_ot_layout=true

Or do not use anti-aliasing and then use bitmap fonts with the following 
settings:

    type_engine=xcore
    use_anti_alias=false
    line_space=0
    use_ot_layout=false

On Debian-based distributions, good fixed-width bitmap terminal fonts are 
available in the `xfonts-75dpi` and `xfonts-100dpi` packages. Use them with the 
second set of options above in the ‘`main`’ configuration file and in the 
‘`font`’ configuration file the following:

    ISO10646_UCS4_1 = -misc-fixed-medium-r-semicondensed-*-13-*-*-*-*-*-iso10646-*;

For anti-aliased fonts, use the first set of options, with the following in the 
‘`aafont`’ configuration file:

    ISO10647_UCS4_1 = DejaVu Sans Mono;
    U+0590-05FF = Ezra SIL;

I also recommend the following settings (all in the ‘`main`’ configuration 
file):

    bidi_mode=left
    bg_color=black
    fg_color=white
    compose_dec_special_font=true
    copy_paste_via_ucs=false
    only_use_unicode_font=true
    receive_string_via_ucs=true
    encoding=UTF-8
    regard_uri_as_word=true
    tabsize=4
    termtype=st-256color
    use_bidi=true
    use_combining=true
    use_ctl=true
    use_scrollbar=false


